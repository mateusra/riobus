package Data;

import Data.DataBase.DataBase;
import Data.Grid.Grid;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author mateus
 */
public class Test {
    public static void main(String[] args) throws IOException {
        DataBase db = new DataBase();
        //db.cleanData();

        Grid grid = db.collectData("371", 10);
        grid.reconstruct();

        StringBuilder json = new StringBuilder("var population = ").append(grid.populationPerElement()).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/population.js"))) {
            out.write(json.toString());
        }
        json = new StringBuilder("var grid = ").append(grid.generateGrid()).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/grid.js"))) {
            out.write(json.toString());
        }
        json = new StringBuilder("var points = ").append(grid.activeElements()).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/points.js"))) {
            out.write(json.toString());
        }

        json = new StringBuilder("var route = ").append(grid.mostUsedRoute()).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/route.js"))) {
            out.write(json.toString());
        }

        json = new StringBuilder("var velocity = ").append(grid.velocityHeatMap()).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/velocity.js"))) {
            out.write(json.toString());
        }
    }

    /*
    public static void main(String[] args) {
        StringBuilder json = new StringBuilder("[");
        try (BufferedReader in = new BufferedReader(new FileReader(new File("route.txt")))) {
            String line = in.readLine();
            Pattern scan = Pattern.compile("\"(.+)\",\"(.+)\"");
            while (line != null) {
                Matcher matcher = scan.matcher(line);
                if (matcher.find()) {
                    json.append(new GeodesicPoint(Double.parseDouble(matcher.group(1)), Double.parseDouble(matcher.group(2)))).append(", ");
                }
                line = in.readLine();
            }
            json.delete(json.length() - 2, json.length()).append("]");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        json = new StringBuilder("var points = ").append(json).append(";");
        try (BufferedWriter out = new BufferedWriter(new FileWriter("web/js/data/points.js"))) {
            out.write(json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */
}