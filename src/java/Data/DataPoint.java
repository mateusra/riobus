package Data;

/**
 * @author mateus
 */
public class DataPoint {

    private double lat, lng, vel;

    public DataPoint(double lat, double lng, double vel) {
        this.lat = lat;
        this.lng = lng;
        this.vel = vel;
    }

    public double latDegrees() {
        return lat;
    }

    public double lngDegrees() {
        return lng;
    }

    public double latRadians() {
        return Math.toRadians(lat);
    }

    public double lngRadians() {
        return Math.toRadians(lng);
    }

    public double vel() {
        return vel;
    }

    public String toString() {
        return String.format("{\"lat\": %f,\"lng\": %f,\"vel\": %f}", lat, lng, vel);
    }
}
