package Data;

import java.util.*;

/**
 * Created by mateus on 11/01/15.
 */
public class Finder implements Runnable {
    private Graph g;
    private int startPoint;

    public Finder(Graph g, int startPoint) {
        this.g = g;
        this.startPoint = startPoint;
    }

    @Override
    public void run() {
        Deque<Integer> nextNodes = new ArrayDeque<>();
        nextNodes.push(startPoint);
        while (!nextNodes.isEmpty()) {
            int nextNode = nextNodes.pop();
            List<Edge> edges = new ArrayList<>();
            for (Iterator<Edge> edgeIterator = g.edgesOf(nextNode); edgeIterator.hasNext(); ) {
                edges.add(edgeIterator.next());
            }
            edges.sort((a, b) -> Integer.compare(b.population(), a.population()));
            edges.removeIf((edge) -> edge.from() == edge.to());
            if (edges.isEmpty() || edges.get(0).isValid()) return;
            edges.removeIf((edge) -> ((edges.get(0).population() - edge.population()) / new Double(edges.get(0).population())) > 0.2);
            if (g.retrieve(edges.get(0).to(), edges.get(0).from()) != null && g.retrieve(edges.get(0).to(), edges.get(0).from()).isValid()) return;
            if (edges.size() == 1) {
                g.retrieve(nextNode, edges.get(0).to()).valid(true);
                nextNodes.push(edges.get(0).to());
            } else {
                for (Edge edge : edges) {
                    g.retrieve(edge.from(), edge.to()).valid(true);
                    nextNodes.push(edge.to());
                }
            }
        }
    }

    private List<Integer> max(Map<Integer, Double> dist) {
        List<Map.Entry<Integer, Double>> sorted = new ArrayList<>(dist.entrySet());
        sorted.sort((a, b) -> b.getValue().compareTo(a.getValue()));
        List<Integer> max = new ArrayList<>();
        for (int i = 1; i < sorted.size(); i++) {
            max.add(sorted.get(i - 1).getKey());
            if ((sorted.get(0).getValue() - sorted.get(i).getValue()) / sorted.get(0).getValue() > 0.2) {
                break;
            }
        }
        return max;
    }
}
