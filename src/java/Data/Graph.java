package Data;

import java.util.*;

/**
 * @author mateus
 */
public class Graph implements Iterable<Integer> {

    private Map<Integer, Set<Edge>> graph;

    public Graph() {
        this.graph = new HashMap<>();
    }

    public void add(int vertex) {
        graph.put(vertex, new HashSet<>());
    }

    public void remove(int vertex) {
        this.graph.keySet().stream().forEach((key) -> this.graph.get(key).remove(new Edge(key, vertex, 0)));
        this.graph.remove(vertex);
    }

    public Edge retrieve(int from, int to) {
        for (Edge edge : this.graph.get(from)) {
            if (edge.to() == (to)) {
                return edge;
            }
        }
        return null;
    }

    public boolean contains(int vertex) {
        return this.graph.keySet().contains(vertex);
    }

    public void connect(int from, int to, int weight) {
        this.graph.get(from).add(new Edge(from, to, weight));
    }

    public void disconnect(int from, int to) {
        this.graph.get(from).remove(new Edge(from, to, 0));
    }

    @Override
    public Iterator<Integer> iterator() {
        return this.graph.keySet().iterator();
    }

    public Iterator<Edge> edgesOf(int vertex) {
        return this.graph.get(vertex).iterator();
    }
}
