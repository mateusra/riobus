package Data;

/**
 * @author mateus
 */
public class Edge {
    private int from, to;
    private int population;
    private boolean valid;

    public Edge(int from, int to, int population) {
        this.from = from;
        this.to = to;
        this.population = population;
        this.valid = false;
    }

    public int to() {
        return to;
    }

    public int from() {
        return from;
    }

    public int population() {
        return population;
    }

    public boolean isValid() {
        return valid;
    }

    public void valid(boolean valid) {
        this.valid = valid;
    }

    public void population(int population) {
        this.population = population;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (to != edge.to) return false;
        if (from != edge.from) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = to;
        result = 31 * result + population;
        result = 31 * result + (valid ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "from=" + from +
                ", to=" + to +
                ", population=" + population +
                ", valid=" + valid +
                '}';
    }
}
