package Data;

/**
 * @author mateus
 */
public class GeodesicPoint {

    private double lat, lng;

    public GeodesicPoint(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double latDegrees() {
        return lat;
    }

    public double lngDegrees() {
        return lng;
    }

    public double latRadians() {
        return Math.toRadians(lat);
    }

    public double lngRadians() {
        return Math.toRadians(lng);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.lat) ^ (Double.doubleToLongBits(this.lat) >>> 32));
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.lng) ^ (Double.doubleToLongBits(this.lng) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeodesicPoint other = (GeodesicPoint) obj;
        if (Double.doubleToLongBits(this.lat) != Double.doubleToLongBits(other.lat)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lng) != Double.doubleToLongBits(other.lng)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return String.format("{\"lat\": %f, \"lng\": %f}", lat, lng);
    }
}
