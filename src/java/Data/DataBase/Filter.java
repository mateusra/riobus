package Data.DataBase;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mateus
 */
public class Filter implements Runnable {

    private File target;

    public Filter(File target) {
        this.target = target;
    }

    @Override
    public void run() {
        Pattern scanPattern = Pattern.compile("\\[\"([^,]+)\",\"([^\"]+)\",([^,]+),([^,]+),([^,]+),([^,]+)\\].");
        Pattern filterPattern = Pattern.compile("\\{busID: ([^,]+), date: ([^,]+), lat: ([^,]+), lng: ([^,]+), vel: ([^\\}]+)\\}");
        File destination = new File(DataBase.RESOURCES.getAbsolutePath() + "/" + target.getName());
        destination.mkdir();
        System.out.println("Analisando Jsons para: " + target.getName());
        SimpleDateFormat DIRECTORY_DATA_FORMAT = new SimpleDateFormat(DataBase.DIRECTORY_DATA_FORMAT);
        SimpleDateFormat JSON_DATA_FORMAT = new SimpleDateFormat(DataBase.JSON_DATA_FORMAT);
        SimpleDateFormat CORRECT_DATA_FORMAT = new SimpleDateFormat(DataBase.CORRECT_DATA_FORMAT);
        for (File json : target.listFiles(DataBase.JSON_FILTER)) {
            Map<String, Set<RawData>> data = new HashMap<>();
            try (BufferedReader in = new BufferedReader(new FileReader(json))) {
                Calendar targetDate = Calendar.getInstance();
                targetDate.setTime(DIRECTORY_DATA_FORMAT.parse(String.format("%s %s:00", target.getName(), json.getName().substring(0, json.getName().lastIndexOf(".json")))));
                StringBuilder content = new StringBuilder(in.readLine());
                content.delete(0, 84).delete(content.length() - 1, content.length());
                Matcher matcher = scanPattern.matcher(content);
                while (matcher.find()) {
                    String date = matcher.group(1), busID = matcher.group(2).replaceAll("\"", "").replaceAll("\\.0", ""), routeID = matcher.group(3).replaceAll("\"", "").replaceAll("\\.0", "");
                    if (!busID.equals("") && !routeID.equals("") && date.length() == 19) {
                        Calendar actualDate = Calendar.getInstance();
                        actualDate.setTime(JSON_DATA_FORMAT.parse(String.format(date)));
                        long diff = (targetDate.getTimeInMillis() - actualDate.getTimeInMillis()) / (60 * 1000);
                        if (targetDate.get(Calendar.DAY_OF_MONTH) == actualDate.get(Calendar.DAY_OF_MONTH) && diff >= 0 && diff <= DataBase.TIME_WINDOW) {
                            if (!data.containsKey(routeID)) {
                                data.put(routeID, new HashSet<>());
                            }
                            data.get(routeID).add(new RawData(busID, actualDate.getTime(), Double.parseDouble(matcher.group(4)), Double.parseDouble(matcher.group(5)), Double.parseDouble(matcher.group(6))));
                        }
                    }
                }
            } catch (ParseException | IOException ex) {
                Logger.getLogger(Filter.class.getName()).log(Level.SEVERE, null, target.getName());
            }

            for (String routeID : data.keySet()) {
                try (BufferedWriter out = new BufferedWriter(new FileWriter(destination.getAbsolutePath() + "/" + routeID + ".json", true))) {
                    StringBuilder jsonData = new StringBuilder(data.get(routeID).toString());
                    jsonData.delete(0, 1).delete(jsonData.length() - 1, jsonData.length()).append(", ");
                    out.write(jsonData.toString());
                } catch (IOException ex) {
                    Logger.getLogger(Filter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("Limpando Arquivos Temporarios para: " + target.getName());
        for (File route : destination.listFiles(DataBase.SIMPLE_JSON_FILTER)) {
            Map<String, Set<CleanData>> data = new HashMap<>();
            try (BufferedReader in = new BufferedReader(new FileReader(route))) {
                StringBuilder json = new StringBuilder(in.readLine());
                Matcher matcher = filterPattern.matcher(json);
                while (matcher.find()) {
                    if (!data.containsKey(matcher.group(1))) {
                        data.put(matcher.group(1), new HashSet<>());
                    }
                    data.get(matcher.group(1)).add(new CleanData(CORRECT_DATA_FORMAT.parse(matcher.group(2)), Double.parseDouble(matcher.group(3)), Double.parseDouble(matcher.group(4)), Double.parseDouble(matcher.group(5))));
                }
            } catch (ParseException | IOException ex) {
                Logger.getLogger(Filter.class.getName()).log(Level.SEVERE, null, ex);
            }
            route.delete();
            try (BufferedWriter out = new BufferedWriter(new FileWriter(route))) {
                StringBuilder json = new StringBuilder("[");
                for (String busID : data.keySet()) {
                    json.append("{\"busID\": \"").append(busID).append("\", \"samples\": [");
                    List<CleanData> sortedData = new ArrayList<>(data.get(busID));
                    sortedData.sort((CleanData a, CleanData b) -> a.date().compareTo(b.date()));
                    sortedData.stream().forEach((rawData) -> {
                        json.append(rawData.toString()).append(", ");
                    });
                    json.delete(json.length() - 2, json.length()).append("]}, ");
                }
                json.delete(json.length() - 2, json.length()).append("]");
                out.write(json.toString());
            } catch (IOException ex) {
                Logger.getLogger(Filter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}