package Data.DataBase;

import Data.Grid.Grid;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mateus
 */
public class DataBase {

    static final int TIME_WINDOW = 15;
    static final String DIRECTORY_DATA_FORMAT = "yyyy-MM-dd HH-mm:ss";
    static final String JSON_DATA_FORMAT = "MM-dd-yyyy HH:mm:ss";
    static final String CORRECT_DATA_FORMAT = "yyyy/MM/dd HH:mm:ss";
    static final File RESOURCES = new File("resources");
    static final File RAW_DATA = new File("raw_data");

    static final FileFilter FOLDER_FILTER = (File folder) -> {
        return folder.isDirectory() && folder.getName().matches("\\d{4}-\\d{2}-\\d{2}") && !Arrays.asList(DataBase.RESOURCES.list()).contains(folder.getName());
    };
    static final FileFilter JSON_FILTER = (File json) -> {
        return json.isFile() && json.length() > 0 && json.canRead() && json.getName().matches("\\d{2}-\\d{2}\\.json");
    };
    static final FileFilter SIMPLE_JSON_FILTER = (File pathname) -> {
        return pathname.getName().endsWith(".json");
    };

    public DataBase() {
        if (!RESOURCES.exists()) {
            RESOURCES.mkdir();
        }
        if (!RAW_DATA.exists()) {
            RAW_DATA.mkdir();
        }
    }

    public String getRoutes() {
        StringBuilder json = new StringBuilder("[");
        Set<String> files = new HashSet<>();
        for (File folder : RESOURCES.listFiles((file) -> file.isDirectory() && file.getName().matches("\\d{4}-\\d{2}-\\d{2}"))) {
            for (File jsonFile : folder.listFiles(SIMPLE_JSON_FILTER)) {
                files.add(jsonFile.getName().substring(0, jsonFile.getName().indexOf(".")));
            }
        }
        files.stream().forEach((String file) -> json.append(file).append(", "));
        json.delete(json.length() - 2, json.length());
        return json.toString();
    }

    public void cleanData() {
        List<Thread> threads = new ArrayList<>();
        for (File folder : RAW_DATA.listFiles(FOLDER_FILTER)) {
            Thread actual = new Thread(new Filter(folder));
            actual.start();
            threads.add(actual);
        }
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Grid collectData(String routeID, double elementSize) {
        Grid grid = new Grid(elementSize);
        List<File> targets = Arrays.asList(RESOURCES.listFiles((File folder) -> folder.isDirectory() && folder.getName().matches("\\d{4}-\\d{2}-\\d{2}")));
        targets.sort((File a, File b) -> a.getName().compareTo(b.getName()));
        List<Thread> threads = new ArrayList<>();
        for (File folder : targets) {
            Thread actual = new Thread(new Collector(routeID, grid, folder));
            actual.start();
            threads.add(actual);
        }
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return grid;
    }
}

class RawData {

    private String busID;
    private Date date;
    private double lat, lng, vel;

    public RawData(String busId, Date date, double lat, double lng, double vel) {
        this.busID = busId;
        this.date = date;
        this.lat = lat;
        this.lng = lng;
        this.vel = vel;
    }

    public String busId() {
        return busID;
    }

    public Date date() {
        return date;
    }

    public double lat() {
        return lat;
    }

    public double lng() {
        return lng;
    }

    public double vel() {
        return vel;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.busID);
        hash = 23 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RawData other = (RawData) obj;
        if (!Objects.equals(this.busID, other.busID)) {
            return false;
        }
        return Objects.equals(this.date, other.date);
    }

    @Override
    public String toString() {
        return String.format("{busID: %s, date: %s, lat: %f, lng: %f, vel: %f}", busID, (new SimpleDateFormat(DataBase.CORRECT_DATA_FORMAT)).format(date), lat, lng, vel);
    }
}

class CleanData {

    private Date date;
    private double lat, lng, vel;

    public CleanData(Date date, double lat, double lng, double vel) {
        this.date = date;
        this.lat = lat;
        this.lng = lng;
        this.vel = vel;
    }

    public Date date() {
        return date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CleanData other = (CleanData) obj;
        return Objects.equals(this.date, other.date);
    }

    @Override
    public String toString() {
        return String.format("{\"time\": \"%s\", \"lat\": %f, \"lng\": %f, \"vel\": %f}", (new SimpleDateFormat(DataBase.CORRECT_DATA_FORMAT)).format(date).split(" ")[1], lat, lng, vel);
    }

}
