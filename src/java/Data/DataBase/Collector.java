package Data.DataBase;

import Data.DataPoint;
import Data.Grid.Grid;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mateus
 */
public class Collector implements Runnable {

    private Grid grid;
    private String route;
    private File folder;

    public Collector(String route, Grid grid, File folder) {
        System.out.println("Collecting: " + folder.getName());
        this.route = route;
        this.grid = grid;
        this.folder = folder;
    }

    public void run() {
        JSONParser parser = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
        File[] target = folder.listFiles((File file) -> file.getName().equals(route + ".json"));
        if (target.length == 0) {
            return;
        }
        try (BufferedReader in = new BufferedReader(new FileReader(target[0]))) {
            List<Map<String, ?>> json = (List<Map<String, ?>>) parser.parse(in);
            for (Map<String, ?> bus : json) {
                List<Map<String, ?>> samples = (List<Map<String, ?>>) bus.get("samples");
                grid.add(new DataPoint((Double) samples.get(0).get("lat"), (Double) samples.get(0).get("lng"), (Double) samples.get(0).get("vel")));
                for (int i = 1; i < samples.size(); i++) {
                    Map<String, ?> previous = samples.get(i - 1);
                    Map<String, ?> actual = samples.get(i);
                    grid.add(new DataPoint((Double) actual.get("lat"), (Double) actual.get("lng"), (Double) actual.get("vel")));
                    grid.connect(new DataPoint((Double) previous.get("lat"), (Double) previous.get("lng"), (Double) previous.get("vel")),
                                 new DataPoint((Double) actual.get("lat"), (Double) actual.get("lng"), (Double) actual.get("vel")));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Collector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
