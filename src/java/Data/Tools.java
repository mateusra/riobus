package Data;

import java.util.Collection;

/**
 * @author mateus
 */
public class Tools {

    private static final double a = 6378160, b = 6356774.719, f = 1 / 298.247167;

    public static double calculateRadius(GeodesicPoint p) {
        double eqR2 = 6378137.0 * 6378137.0, pR2 = 6356752.3 * 6356752.3;
        double cosLat2 = Math.pow(Math.cos(p.latRadians()), 2), sinLat2 = Math.pow(Math.sin(p.latRadians()), 2);
        double radius = Math.sqrt((eqR2 * eqR2 * cosLat2 + pR2 * pR2 * sinLat2) / (eqR2 * cosLat2 + pR2 * sinLat2));
        return radius;
    }

    public static double calculateRadius(GeodesicPoint a, GeodesicPoint... points) {
        double radius = Tools.calculateRadius(a);
        for (GeodesicPoint point : points) {
            radius += Tools.calculateRadius(point);
        }
        return radius / (points.length + 1);
    }
    public static double calculateDistance(GeodesicPoint a, GeodesicPoint b) {
        double L = b.lngRadians() - a.lngRadians();
        double tanU1 = (1 - f) * Math.tan(a.latRadians()), cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1;
        double tanU2 = (1 - f) * Math.tan(b.latRadians()), cosU2 = 1 / Math.sqrt((1 + tanU2 * tanU2)), sinU2 = tanU2 * cosU2;

        double λ = L, λʹ, iterationLimit = 100;
        double sinα, cosSqα, cos2σM, sinσ, σ, cosσ;
        do {
            double sinλ = Math.sin(λ), cosλ = Math.cos(λ);
            double sinSqσ = (cosU2 * sinλ) * (cosU2 * sinλ) + (cosU1 * sinU2 - sinU1 * cosU2 * cosλ) * (cosU1 * sinU2 - sinU1 * cosU2 * cosλ);
            sinσ = Math.sqrt(sinSqσ);
            if (sinσ == 0) {
                return 0;  // co-incident points
            }
            cosσ = sinU1 * sinU2 + cosU1 * cosU2 * cosλ;
            σ = Math.atan2(sinσ, cosσ);
            sinα = cosU1 * cosU2 * sinλ / sinσ;
            cosSqα = 1 - sinα * sinα;
            cos2σM = cosσ - 2 * sinU1 * sinU2 / cosSqα;
            if (Double.isNaN(cos2σM)) {
                cos2σM = 0;  // equatorial line: cosSqα=0 (§6)
            }
            double C = f / 16 * cosSqα * (4 + f * (4 - 3 * cosSqα));
            λʹ = λ;
            λ = L + (1 - C) * f * sinα * (σ + C * sinσ * (cos2σM + C * cosσ * (-1 + 2 * cos2σM * cos2σM)));
        } while (Math.abs(λ - λʹ) > 1e-12 && --iterationLimit > 0);
        double uSq = cosSqα * (Tools.a * Tools.a - Tools.b * Tools.b) / (Tools.b * Tools.b);
        double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
        double Δσ = B * sinσ * (cos2σM + B / 4 * (cosσ * (-1 + 2 * cos2σM * cos2σM)
                - B / 6 * cos2σM * (-3 + 4 * sinσ * sinσ) * (-3 + 4 * cos2σM * cos2σM)));

        return Tools.b * A * (σ - Δσ);
    }

    public static GeodesicPoint barycenterOf(Collection<? extends GeodesicPoint> points) {
        double lat = 0, lng = 0;
        for (GeodesicPoint point : points) {
            lat += point.latDegrees();
            lng += point.lngDegrees();
        }
        return new GeodesicPoint(lat / points.size(), lng / points.size());
    }

    public static double calculateDistanceOld(GeodesicPoint a, GeodesicPoint b) {
        double k = Math.pow(Math.sin((b.latRadians() - a.latRadians()) / 2), 2) + Math.cos(a.latRadians()) * Math.cos(b.latRadians()) * Math.pow(Math.sin((b.lngRadians() - a.lngRadians()) / 2), 2);
        double q = 2 * Math.atan2(Math.sqrt(k), Math.sqrt(1 - k));
        return Tools.calculateRadius(a, b) * q;
    }

    public static double calculateBearing(GeodesicPoint a, GeodesicPoint b) {
        double x = Math.sin(b.lngRadians() - a.lngRadians()) * Math.cos(b.latRadians());
        double y = Math.cos(a.latRadians()) * Math.sin(b.latRadians()) - Math.sin(a.latRadians()) * Math.cos(b.latRadians()) * Math.cos(b.lngRadians() - a.lngRadians());
        return Math.atan2(y, x);
    }

    public static GeodesicPoint calculateDestination(GeodesicPoint point, double bearing, double distance) {
        double sinα1 = Math.sin(bearing);
        double cosα1 = Math.cos(bearing);

        double tanU1 = (1 - f) * Math.tan(point.latRadians()), cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1;
        double σ1 = Math.atan2(tanU1, cosα1);
        double sinα = cosU1 * sinα1;
        double cosSqα = 1 - sinα * sinα;
        double uSq = cosSqα * (a * a - b * b) / (b * b);
        double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

        double σ = distance / (b * A), σʹ;
        double cos2σM, sinσ, cosσ, Δσ;
        do {
            cos2σM = Math.cos(2 * σ1 + σ);
            sinσ = Math.sin(σ);
            cosσ = Math.cos(σ);
            Δσ = B * sinσ * (cos2σM + B / 4 * (cosσ * (-1 + 2 * cos2σM * cos2σM)
                    - B / 6 * cos2σM * (-3 + 4 * sinσ * sinσ) * (-3 + 4 * cos2σM * cos2σM)));
            σʹ = σ;
            σ = distance / (b * A) + Δσ;
        } while (Math.abs(σ - σʹ) > 1e-12);

        double tmp = sinU1 * sinσ - cosU1 * cosσ * cosα1;
        double φ2 = Math.atan2(sinU1 * cosσ + cosU1 * sinσ * cosα1, (1 - f) * Math.sqrt(sinα * sinα + tmp * tmp));
        double λ = Math.atan2(sinσ * sinα1, cosU1 * cosσ - sinU1 * sinσ * cosα1);
        double C = f / 16 * cosSqα * (4 + f * (4 - 3 * cosSqα));
        double L = λ - (1 - C) * f * sinα
                * (σ + C * sinσ * (cos2σM + C * cosσ * (-1 + 2 * cos2σM * cos2σM)));
        double λ2 = (point.lngRadians() + L + 3 * Math.PI) % (2 * Math.PI) - Math.PI;  // normalise to -180...+180
        return new GeodesicPoint(Math.toDegrees(φ2), Math.toDegrees(λ2));
    }

    public static GeodesicPoint calculateDestinationOld(GeodesicPoint point, double bearing, double distance) {
        double radius = distance / calculateRadius(point);
        double lat = Math.asin(Math.sin(point.latRadians()) * Math.cos(radius) + Math.cos(point.latRadians()) * Math.sin(radius) * Math.cos(bearing));
        double lng = point.lngRadians() + Math.atan2(Math.sin(bearing) * Math.sin(radius) * Math.cos(point.latRadians()), Math.cos(radius) - Math.sin(point.latRadians()) * Math.sin(lat));
        return new GeodesicPoint(Math.toDegrees(lat), Math.toDegrees(lng));
    }
}
