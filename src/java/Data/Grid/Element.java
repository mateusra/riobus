package Data.Grid;

import Data.DataPoint;

/**
 * Created by mateus on 26/01/15.
 */
public class Element {

    private int id, population;
    private double lat, lng, vel;

    public Element(int id) {
        this.id = id;
        this.lat = 0;
        this.lng = 0;
        this.vel = 0;
        this.population = 0;
    }

    public int id() {
        return id;
    }

    public void add(DataPoint point) {
        add(point, 1);
    }

    public void add(DataPoint point, int weight) {
        this.lat += point.latDegrees() * weight;
        this.lng += point.lngDegrees() * weight;
        this.vel += point.vel() * weight;
        this.population += weight;
    }

    public int population() {
        return this.population;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Element other = (Element) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public DataPoint location() {
        return new DataPoint(lat / population, lng / population, vel / population);
    }

    @Override
    public String toString() {
        return "{\"location\": " + location() + ", \"population\": " + population + "}";
    }
}