package Data.Grid;

/**
 * Created by mateus on 02/02/15.
 */
public class WayPoint extends Point {

    private Point next;

    public WayPoint(double lat, double lng) {
        super(lat, lng);
        this.next = null;
    }

    @Override
    public void connectTo(Point next) {
        this.next = next;
    }

    @Override
    public String route() {
        return super.route() + (this.next == null ? "" : next.route());
    }
}
