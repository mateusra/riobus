package Data.Grid;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by mateus on 02/02/15.
 */
public class Intersection extends Point {

    private Collection<Point> adj;

    public Intersection(double lat, double lng) {
        super(lat, lng);
        this.adj = new HashSet<>();
    }

    @Override
    public void connectTo(Point next) {
        this.adj.add(next);
    }

    @Override
    public String route() {
        StringBuilder json = new StringBuilder(super.route());
        for (Point point : adj) {
            json.append(point.route());
        }
        return json.toString();
    }
}
