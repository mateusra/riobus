package Data.Grid;

/**
 * Created by mateus on 02/02/15.
 */
public abstract class Point {
    private double lat, lng;

    public Point(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public abstract void connectTo(Point next);

    public String route() {
        return "{\"lat\":" + lat + ", \"lng\":" + lng + "}, ";
    }

}
