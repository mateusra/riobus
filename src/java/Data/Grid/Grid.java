package Data.Grid;

import Data.DataPoint;
import Data.Tools;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author mateus
 */
public class Grid extends IGrid {
    private Map<Integer, Map<Integer, Integer>> graph;
    private Map<Integer, Element> activeElements;

    public Grid(double distance) {
        super(distance);
        this.graph = new ConcurrentHashMap<>();
        this.activeElements = new ConcurrentHashMap<>();
    }

    public void reconstruct() {
        double x = 0;
        int n = 0;
        for (Element actual : activeElements.values()) {
            if (actual.population() != 1) {
                x += actual.population();
                n++;
            }
        }
        x /= n;
        x *= 0.5;
        final double media = x;
        activeElements.forEach((k, v) -> activeElements.computeIfPresent(k, (id, element) -> {
            if (element.population() < media) {
                element = null;
            } else {
                Collection<Integer> adj = adj(element.location());
                adj.removeIf((actual) -> !activeElements.containsKey(actual));
                if (adj.isEmpty()) element = null;
            }
            if (element == null) {
                Map<Integer, Integer> removed = graph.remove(id);
                removed.remove(id);
                graph.forEach((from, edges) -> {
                    if (edges.containsKey(id)) {
                        edges.remove(id);
                    }
                });
            }
            return element;
        }));
        graph.forEach((from, edges) -> {
            double f = 0;
            for (Integer w : edges.values()) {
                if (f < w) f = w;
            }
            final double max = f;
            Set<Integer> toRemove = new HashSet<>();
            edges.forEach((to, weight) -> {
                if (weight / max < 0.5) {
                    toRemove.add(to);
                }
            });
            toRemove.forEach(edges::remove);
        });
    }

    public Set<Integer> connectComponent() {
        final Set<Integer> max = new HashSet<>();
        graph.forEach((node, edges) -> {
            Set<Integer> cc = new HashSet<>();
            Queue<Integer> toVeref = new ArrayDeque<>();
            cc.add(node);
            toVeref.add(node);
            while (!toVeref.isEmpty() && cc.size() < graph.size()) {
                edges = graph.get(toVeref.remove());
                edges.forEach((to, weight) -> {
                    if (cc.add(to)) toVeref.add(to);
                });
            }
            if (max.size() < cc.size()) {
                max.clear();
                max.addAll(cc);
            }
        });
        return max;
    }


    public void join(int factor) {
        Map<Integer, Map<Integer, Integer>> g = graph;
        Map<Integer, Element> a = activeElements;
        this.graph = new ConcurrentHashMap<>();
        this.activeElements = new ConcurrentHashMap<>();
        super.distance(super.distance() * factor);
        super.nElements((int) (Tools.calculateDistance(IGrid.a, b) / super.distance()));
        for (Element actual : a.values()) this.add(actual.location(), 1);
        g.forEach((from, edges) -> edges.forEach((to, weight) -> this.connect(a.get(from).location(), a.get(to).location(), weight)));
    }

    @Override
    public void add(DataPoint point) {
        add(point, 1);
    }

    @Override
    public void add(DataPoint point, int weight) {
        int id = this.calculateElement(point);
        graph.putIfAbsent(id, new HashMap<>());
        activeElements.putIfAbsent(id, new Element(id));
        activeElements.get(id).add(point, weight);
    }

    @Override
    public void connect(DataPoint a, DataPoint b) {
        connect(a, b, 1);
    }

    @Override
    public void connect(DataPoint a, DataPoint b, int weight) {
        int id = this.calculateElement(b);
        this.graph.get(this.calculateElement(a)).putIfAbsent(id, 0);
        this.graph.get(this.calculateElement(a)).computeIfPresent(id, (key, value) -> value + weight);
    }

    @Override
    public String populationPerElement() {
        StringBuilder json = new StringBuilder("[");
        for (Element actual : activeElements.values())
            json.append("{\"lat\":").append(actual.location().latDegrees())
                .append(", \"lng\":").append(actual.location().lngDegrees())
                .append(", \"population\":").append(actual.population()).append("}, ");
        json.delete(json.length() - 2, json.length()).append("]");
        return json.toString();
    }

    @Override
    public String mostUsedRoute() {
        Map<Integer, Integer> route = new HashMap<>();
        graph.forEach((from, edges) -> {
            Queue<Map.Entry<Integer, Integer>> e = new PriorityQueue<>((a, b) -> b.getValue().compareTo(a.getValue()));
            e.addAll(edges.entrySet());
            e.removeIf((x) -> x.getKey() == from);
            if (!e.isEmpty()) {
                int max = e.remove().getKey();
                if (!Objects.equals(route.get(max), from))
                    route.put(from, max);
            }
        });

        StringBuilder json = new StringBuilder("[");
        route.forEach((k, v) -> {
            json.append("{\"from\": ").append(activeElements.get(k).location().toString())
                .append(", \"to\": ").append(activeElements.get(v).location().toString()).append("}, ");
        });
        json.delete(json.length() - 2, json.length()).append("]");
        return json.toString();
    }


    @Override
    public String activeElements() {
        StringBuilder json = new StringBuilder("[");
        for (Element actual : activeElements.values()) {
            json.append(actual.location().toString()).append(", ");
        }
        json.delete(json.length() - 2, json.length()).append("]");
        return json.toString();
    }

    public String velocityHeatMap() {
        StringBuilder json = new StringBuilder("[");
        for (Element actual : activeElements.values())
            json.append("{\"lat\":").append(actual.location().latDegrees())
                .append(", \"lng\":").append(actual.location().lngDegrees())
                .append(", \"vel\":").append(actual.location().vel()).append("}, ");
        json.delete(json.length() - 2, json.length()).append("]");
        return json.toString();
    }
}