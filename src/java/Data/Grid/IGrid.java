package Data.Grid;

import Data.DataPoint;
import Data.GeodesicPoint;
import Data.Tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mateus on 26/01/15.
 */
public abstract class IGrid {
    protected static final GeodesicPoint c = new Data.GeodesicPoint(-22.6406, -43.7278), d = new GeodesicPoint(-22.6406, -43.1509);
    protected static final GeodesicPoint a = new GeodesicPoint(-23.0737, -43.7278), b = new GeodesicPoint(-23.0737, -43.1509);
    private double distance;
    private int nElements;

    public IGrid(double distance) {
        this.distance = distance;
        this.nElements = (int) (Tools.calculateDistance(a, b) / distance);
    }

    public double distance() {
        return distance;
    }

    public void distance(double distance) {
        this.distance = distance;
    }

    public abstract void add(DataPoint point);

    public abstract void add(DataPoint point, int weight);

    public abstract void connect(DataPoint a, DataPoint b);

    public abstract void connect(DataPoint a, DataPoint b, int weight);

    public abstract String populationPerElement();

    public abstract String mostUsedRoute();

    public abstract String activeElements();

    protected void nElements(int nElements) {
        this.nElements = nElements;
    }

    protected int calculateElement(DataPoint point) {
        int x = (int) (Tools.calculateDistance(a, new GeodesicPoint(a.latDegrees(), point.lngDegrees())) / distance());
        int y = (int) (Tools.calculateDistance(a, new GeodesicPoint(point.latDegrees(), a.lngDegrees())) / distance());
        return y * nElements + x;
    }

    protected Collection<Integer> adj(DataPoint x) {
        Collection<Integer> adj = new ArrayList<>();
        GeodesicPoint p = Tools.calculateDestination(new GeodesicPoint(x.latDegrees(), x.lngDegrees()), 0, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(p, Math.PI / 2, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(p, 3 * Math.PI / 2, 2 * this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(new GeodesicPoint(x.latDegrees(), x.lngDegrees()), Math.PI / 2, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(new GeodesicPoint(x.latDegrees(), x.lngDegrees()), Math.PI, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(p, Math.PI / 2, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(p, 3 * Math.PI / 2, 2 * this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));

        p = Tools.calculateDestination(new GeodesicPoint(x.latDegrees(), x.lngDegrees()), 3 * Math.PI / 2, this.distance());
        adj.add(calculateElement(new DataPoint(p.latDegrees(), p.lngDegrees(), 0)));
        adj.remove(calculateElement(x));
        return adj;
    }

    public final String generateGrid() {
        List<GeodesicPoint> axisSouth = new ArrayList<>();
        GeodesicPoint actual = a;
        do {
            axisSouth.add(actual);
            actual = Tools.calculateDestination(actual, Math.PI / 2, distance);
        } while (actual.lngDegrees() < b.lngDegrees());
        axisSouth.add(b);

        List<GeodesicPoint> axisNorth = new ArrayList<>();
        actual = c;
        do {
            axisNorth.add(actual);
            actual = Tools.calculateDestination(actual, Math.PI / 2, distance);
        } while (actual.lngDegrees() < d.lngDegrees());
        axisNorth.add(d);

        List<GeodesicPoint> axisWest = new ArrayList<>();
        actual = a;
        do {
            axisWest.add(actual);
            actual = Tools.calculateDestination(actual, 0, distance);
        } while (actual.latDegrees() < c.latDegrees());
        axisWest.add(c);

        List<GeodesicPoint> axisEast = new ArrayList<>();
        actual = b;
        do {
            axisEast.add(actual);
            actual = Tools.calculateDestination(actual, 0, distance);
        } while (actual.latDegrees() < d.latDegrees());
        axisEast.add(d);

        StringBuilder json = new StringBuilder("[");
        for (int i = 0; i < Math.min(axisEast.size(), axisWest.size()); i++) {
            json.append("{\"from\": ").append(axisEast.get(i).toString()).append(", \"to\": ").append(axisWest.get(i).toString()).append("}, ");
        }

        for (int i = 0; i < Math.min(axisNorth.size(), axisSouth.size()); i++) {
            json.append("{\"from\": ").append(axisNorth.get(i).toString()).append(", \"to\": ").append(axisSouth.get(i).toString()).append("}, ");
        }
        json.delete(json.length() - 2, json.length()).append("]");
        return json.toString();
    }
}
