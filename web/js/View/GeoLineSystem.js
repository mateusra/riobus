/**
 * Created by mateus on 02/02/15.
 */
function GeoLineSystem(canvas, map, geoLines) {
    THREELayer.call(this, canvas, map);
    this.geoLines = geoLines;
    this.lines = new Array(this.geoLines.length);
    for (var i = 0; i < this.geoLines.length; i++) {
        var line = this.geoLines[i];
        var geometry = new THREE.Geometry();
        var geodesicPoint = new google.maps.LatLng(line.from.lat, line.from.lng);
        var point = this.geodesicPointToCartesianPoint(geodesicPoint, this.map);

        geometry.vertices.push(new THREE.Vector3(point.x, point.y, 0));

        geodesicPoint = new google.maps.LatLng(line.to.lat, line.to.lng);
        point = this.geodesicPointToCartesianPoint(geodesicPoint, map);

        geometry.vertices.push(new THREE.Vector3(point.x, point.y, 0));
        this.lines[i] = geometry;
    }
    this.lineStyle();
}

GeoLineSystem.prototype = Object.create(THREELayer.prototype);
GeoLineSystem.prototype.constructor = GeoLineSystem;

GeoLineSystem.prototype.update = function () {
    if (this.mustUpdate) {
        for (var i = 0; i < this.lines.length; i++) {
            var line = this.geoLines[i];
            var geometry = this.lines[i];
            var geodesicPoint = new google.maps.LatLng(line.from.lat, line.from.lng);
            var point = this.geodesicPointToCartesianPoint(geodesicPoint);

            geometry.vertices[0].set(point.x, point.y, 0);
            geodesicPoint = new google.maps.LatLng(line.to.lat, line.to.lng);
            point = this.geodesicPointToCartesianPoint(geodesicPoint);

            geometry.vertices[1].set(point.x, point.y, 0);
            geometry.verticesNeedUpdate = true;
        }
    }
    THREELayer.prototype.update.call(this);
};


GeoLineSystem.prototype.lineStyle = function () {
    throw 'OVERWRITE THIS METHOD!'
};