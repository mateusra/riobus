/**
 * Created by mateus on 02/02/15.
 */
function GeoPointCloud(canvas, map, geoPoints) {
    THREELayer.call(this, canvas, map);
    this.geoPoints = geoPoints;
    this.points = new THREE.Geometry();
    for (var i = 0; i < this.geoPoints.length; i++) {
        var geodesicPoint = new google.maps.LatLng(this.geoPoints[i].lat, this.geoPoints[i].lng);
        var point = this.geodesicPointToCartesianPoint(geodesicPoint);
        this.points.vertices.push(new THREE.Vector3(point.x, point.y, 0));
    }
    this.scene.add(new THREE.PointCloud(this.points, new THREE.PointCloudMaterial({
        color: 0x00FF00,
        map: THREE.ImageUtils.loadTexture('particle.png'),
        size: 15,
        sizeAttenuation: false,
        blending: THREE.AdditiveBlending,
        transparent: true
    })));
}

GeoPointCloud.prototype = Object.create(THREELayer.prototype);
GeoPointCloud.prototype.constructor = GeoPointCloud;

GeoPointCloud.prototype.update = function () {
    if (this.mustUpdate) {
        for (var i = 0; i < this.geoPoints.length; i++) {
            var geodesicPoint = new google.maps.LatLng(this.geoPoints[i].lat, this.geoPoints[i].lng);
            var point = this.geodesicPointToCartesianPoint(geodesicPoint);
            this.points.vertices[i].set(point.x, point.y, 0);
        }
        this.points.verticesNeedUpdate = true;
    }
    THREELayer.prototype.update.call(this);
};
