/**
 * Created by mateus on 11/01/15.
 */
function ViewLayer(map) {
    this.map = map;
    this.mustUpdate = false;
}

ViewLayer.prototype.geodesicPointToCartesianPoint = function (geodesicPoint) {
    var topRight = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getNorthEast());
    var bottomLeft = this.map.getProjection().fromLatLngToPoint(this.map.getBounds().getSouthWest());
    var scale = Math.pow(2, this.map.getZoom());
    var worldPoint = this.map.getProjection().fromLatLngToPoint(geodesicPoint);
    return {x: (worldPoint.x - bottomLeft.x) * scale, y: (worldPoint.y - topRight.y) * scale};
};

ViewLayer.prototype.onOff = function () {
    this.mustUpdate = !this.mustUpdate;
};

ViewLayer.prototype.update = function () {
    throw 'OVERWRITE THIS METHOD!'
};