/**
 * Created by mateus on 02/02/15.
 */

function Grid(canvas, map, geoLines) {
    GeoLineSystem.call(this, canvas, map, geoLines);
}

Grid.prototype = Object.create(GeoLineSystem.prototype);
Grid.prototype.constructor = Grid;

Grid.prototype.lineStyle = function () {
    var lightLineMaterial = new THREE.LineBasicMaterial({color: 0x000000, linewidth: 1, opacity: 0.3});
    var heavyLineMaterial = new THREE.LineBasicMaterial({color: 0x000000, linewidth: 3, opacity: 0.7});
    for (var i = 0; i < this.lines.length; i++) {
        if (i % 10 == 0)
            this.scene.add(new THREE.Line(this.lines[i], heavyLineMaterial));
        else
            this.scene.add(new THREE.Line(this.lines[i], lightLineMaterial));
    }
};