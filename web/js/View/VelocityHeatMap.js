/**
 * Created by mateus on 02/02/15.
 */
function VelocityHeatMap(canvas, data, map) {
    HeatMap.call(this, canvas, data, map);
}

VelocityHeatMap.prototype = Object.create(HeatMap.prototype);
VelocityHeatMap.prototype.constructor = VelocityHeatMap;

VelocityHeatMap.prototype.update = function () {
    this.heatMap.clear();
    this.heatMap.adjustSize();
    if (this.mustUpdate) {
        var max = -1;
        for (var i = 0; i < this.data.length; i++) {
            var geodesicPoint = new google.maps.LatLng(this.data[i].lat, this.data[i].lng);
            if (this.map.getBounds().contains(geodesicPoint)) {
                if (max < this.data[i].vel)
                    max = this.data[i].vel;
            }
        }
        console.log(max);
        for (var i = 0; i < this.data.length; i++) {
            var geodesicPoint = new google.maps.LatLng(this.data[i].lat, this.data[i].lng);
            if (this.map.getBounds().contains(geodesicPoint) && this.data[i].vel == 0) {
                console.log(this.data[i].vel);
                var point = this.geodesicPointToCartesianPoint(geodesicPoint);
                var x = this.data[i].vel / max;
                x = Math.min(Math.max(x, 0), 1);
                this.heatMap.addPoint(point.x, point.y, 40, 0.2);
            }
        }
    }
    this.heatMap.blur();
    this.heatMap.update();
    this.heatMap.display();
};