/**
 * Created by mateus on 02/02/15.
 */
function Route(canvas, map, geoLines) {
    GeoLineSystem.call(this, canvas, map, geoLines);
}

Route.prototype = Object.create(GeoLineSystem.prototype);
Route.prototype.constructor = Route;

Route.prototype.lineStyle = function () {
    var lineMaterial = new THREE.LineBasicMaterial({color: 0xFF0000, linewidth: 5, opacity: 0.7});
    for (var i = 0; i < this.lines.length; i++)
        this.scene.add(new THREE.Line(this.lines[i], lineMaterial));
};