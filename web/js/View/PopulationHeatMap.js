/**
 * Created by mateus on 02/02/15.
 */
function PopulationHeatMap(canvas, data, map) {
    HeatMap.call(this, canvas, data, map);
}

PopulationHeatMap.prototype = Object.create(HeatMap.prototype);
PopulationHeatMap.prototype.constructor = PopulationHeatMap;

PopulationHeatMap.prototype.update = function () {
    this.heatMap.clear();
    this.heatMap.adjustSize();
    if (this.mustUpdate) {
        var max = -1;
        for (var i = 0; i < this.data.length; i++) {
            var geodesicPoint = new google.maps.LatLng(this.data[i].lat, this.data[i].lng);
            if (this.map.getBounds().contains(geodesicPoint)) {
                if (max < this.data[i].population)
                    max = this.data[i].population;
            }
        }
        for (var i = 0; i < this.data.length; i++) {
            var geodesicPoint = new google.maps.LatLng(this.data[i].lat, this.data[i].lng);
            if (this.map.getBounds().contains(geodesicPoint)) {
                var point = this.geodesicPointToCartesianPoint(geodesicPoint);
                var x = this.data[i].population / max;
                x = Math.min(Math.max(x, 0), 1);
                this.heatMap.addPoint(point.x, point.y, 3 * this.map.getZoom(), x);
            }
        }
    }
    this.heatMap.update();
    this.heatMap.display();
};