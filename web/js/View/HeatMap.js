function HeatMap(canvas, data, map) {
    ViewLayer.call(this, map);
    this.data = data;
    this.heatMap = createWebGLHeatmap({canvas: canvas, alphaRange: [0, 1.5], gradientTexture: 'reverse.png'});
}

HeatMap.prototype = Object.create(ViewLayer.prototype);
HeatMap.prototype.constructor = HeatMap;

