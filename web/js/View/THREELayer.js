/**
 * Created by mateus on 12/01/15.
 */
function THREELayer(canvas, map) {
    ViewLayer.call(this, map);
    this.canvas = canvas;
    this.renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true, alpha: true});
    this.renderer.setClearColor(0x000000, 0);
    this.renderer.setSize(canvas.clientWidth, canvas.clientHeight);
    this.scene = new THREE.Scene();
    this.camera = new THREE.OrthographicCamera(0, canvas.clientWidth, 0, canvas.clientHeight, 1, 1000);
    this.camera.position.set(0, 0, 10);
    this.camera.lookAt(this.scene.position);
    this.scene.add(this.camera);
}

THREELayer.prototype = Object.create(ViewLayer.prototype);
THREELayer.prototype.constructor = THREELayer;

THREELayer.prototype.update = function () {
    if (this.mustUpdate)
        this.renderer.render(this.scene, this.camera);
    else
        this.renderer.render(new THREE.Scene(), this.camera);
};
