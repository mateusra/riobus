/**
 * Created by mateus on 11/01/15.
 */
var populationLayer;
var velocityLayer;
var gridLayer;
var routeLayer;
var samplesLayer;
var map;
function main() {
    var mapOptions = {
        zoom: 12,
        center: new google.maps.LatLng(-22.860470, -43.355469),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById('map');
    map = new google.maps.Map(mapDiv, mapOptions);
    //populationLayer = new PopulationHeatMap(document.getElementById('heatmap'), population, map);
    velocityLayer = new VelocityHeatMap(document.getElementById('heatmap'), velocity, map);
    google.maps.event.addListener(map, 'idle', function () {
        google.maps.event.addListener(map, 'bounds_changed', function () {
            velocityLayer.update();
        });
        /*
         gridLayer = new Grid(document.getElementById('grid'), map, grid);
         google.maps.event.addListener(map, 'bounds_changed', function () {
         gridLayer.update();
         });
         */

        /*
         routeLayer = new Route(document.getElementById('route'), map, route);
         google.maps.event.addListener(map, 'bounds_changed', function () {
         routeLayer.update();
         });
         */
        samplesLayer = new GeoPointCloud(document.getElementById('samples'), map, points);
        google.maps.event.addListener(map, 'bounds_changed', function () {
            samplesLayer.update();
        });
        velocityLayer.onOff();
        google.maps.event.clearListeners(map, 'idle');
    });
}

function gridOnOFF() {
    gridLayer.onOff();
    gridLayer.update();
}

function routeOnOff() {
    routeLayer.onOff();
    routeLayer.update();
}

function samplesOnOff() {
    samplesLayer.onOff();
    samplesLayer.update();
}
function heatMapOnOff() {
    heatMap.onOff();
    heatMap.update();
}