var webSocket;
var map;
var gridLayer;
var heatmapLayer;
var routeLayer;
var waiting = false;
var nextData = 'EMPTY';
var route;
var gridOn = true, heatmapOn = false, routeOn = true;

var update;
function main() {
    loadMap();
    //loadTHREELaye();
    //openWebSocket();
}


function loadMap() {
    var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(-22.860470, -43.355469),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById('map');
    map = new google.maps.Map(mapDiv, mapOptions);
    google.maps.event.addListener(map, 'bounds_changed', drawHeatmap);

}

function fromLatLngToPoint(latLng) {
    var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    var scale = Math.pow(2, map.getZoom());
    var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
    return {x: (worldPoint.x - bottomLeft.x) * scale, y: (worldPoint.y - topRight.y) * scale};
}

function loadTHREELaye() {

}

function load() {
    select = document.getElementById('routes');
    console.log(select.options[select.selectedIndex].value);
    console.log(parseFloat(document.getElementById('gridSize').value));
}

function loadGrid() {
    gridOn = !gridOn;
    gridLayer = new ThreejsLayer({map: map}, drawGrid);
}

function loadRoute() {
    routeOn = !routeOn;
    //routeLayer = new ThreejsLayer({map: map}, drawRoute);
}

function loadHeatmap() {
    heatmapOn = !heatmapOn;
    drawHeatmap();
}

function drawGrid(layer) {
    var lineLightMaterial = new THREE.LineBasicMaterial({
        color: 0x722b00,
        opacity: 0.4,
        linewidth: 1.5
    });

    var lineMediumMaterial = new THREE.LineBasicMaterial({
        color: 0x722b00,
        opacity: 0.5,
        linewidth: 2.5
    });

    var lineHeavyMaterial = new THREE.LineBasicMaterial({
        color: 0x722b00,
        opacity: 1,
        linewidth: 3.5
    });

    for (var i = 0; i < grid.length; i += 100) {
        var geometry = new THREE.Geometry();
        var location = new google.maps.LatLng(grid[i].from.lat, grid[i].from.lng);
        var vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        location = new google.maps.LatLng(grid[i].to.lat, grid[i].to.lng);
        vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        layer.add(new THREE.Line(geometry, lineHeavyMaterial));
    }

    for (var i = 0; i < grid.length; i += 10) {
        var geometry = new THREE.Geometry();
        var location = new google.maps.LatLng(grid[i].from.lat, grid[i].from.lng);
        var vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        location = new google.maps.LatLng(grid[i].to.lat, grid[i].to.lng);
        vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);
        if (i % 100 !== 0) {
            layer.add(new THREE.Line(geometry, lineMediumMaterial));
        }
    }

    for (var i = 0; i < grid.length; i++) {
        var geometry = new THREE.Geometry();
        var location = new google.maps.LatLng(grid[i].from.lat, grid[i].from.lng);
        var vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        location = new google.maps.LatLng(grid[i].to.lat, grid[i].to.lng);
        vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        if (i % 10 !== 0) {
            layer.add(new THREE.Line(geometry, lineLightMaterial));
        }
    }
}
function drawHeatmap() {
    if (heatmapOn) {
        if (!heatmapLayer) {
            heatmapLayer = h337.create({
                container: document.getElementById('heatmap'),
                maxOpacity: .5,
                minOpacity: 0,
                blur: 0.5
            });
        }
        var data = heatmap.data;
        var length = data.length;
        var convertHeatmap = {max: heatmap.max, data: []};
        var max = 0;
        for (var i = 0; i < length; i++) {
            if (map.getBounds().contains(new google.maps.LatLng(data[i][0], data[i][1]))) {
                var xy = fromLatLngToPoint(new google.maps.LatLng(data[i][0], data[i][1]));
                convertHeatmap.data.push({x: xy.x, y: xy.y, value: data[i][2]});
                if (max < data[i][2])
                    max = data[i][2];
            }
        }
        convertHeatmap.max = max;
        heatmapLayer.setData(convertHeatmap);
    } else {
        if (heatmapLayer)
            heatmapLayer.setData({max: 1, data: []});
    }
}
function drawRoute(layer) {
    var lineMaterial = new THREE.LineBasicMaterial({
        color: 0xff0000,
        opacity: 1,
        linewidth: 6
    });

    var lenght = routeStatus.length;

    for (var i = 0; i < lenght; i++) {
        var line = routeStatus[i];
        var geometry = new THREE.Geometry();
        var location = new google.maps.LatLng(line.from.lat, line.from.lng);
        var vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);

        location = new google.maps.LatLng(line.to.lat, line.to.lng);
        vertex = layer.fromLatLngToVertex(location);
        geometry.vertices.push(vertex);
        layer.add(new THREE.Line(geometry, lineMaterial));
    }
}

function openWebSocket() {
    if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
        writeResponse("WebSocket is already opened.");
        return;
    }
    webSocket = new WebSocket("ws://localhost:8080/RioBus/socket");

    webSocket.onopen = onOpen;

    webSocket.onmessage = onMessage;

    webSocket.onclose = onClose;
}

function onOpen(event) {
    if (event.data === undefined)
        return;
    send(CONNECT);
}

function onMessage(event) {
    if (waiting) {
        if (nextData === 'EMPTY') {
            nextData = event.data;
        } else {
            if (nextData === ROUTE_LIST) {
                routeList = JSON.parse(event.data);
                select = document.getElementById('routes');
                for (var i = 0; i < routeList.length; i++) {
                    route = document.createElement('option');
                    route.text = routeList[i].routeID;
                    route.value = routeList[i].routeID;
                    select.appendChild(route);
                }
            } else if (nextData === GRID) {
                grid = JSON.parse(event.data);
            } else if (nextData === HEATMAP) {
                heatmap = JSON.parse(event.data);
            } else if (nextData === ROUTE_SAMPLES) {
                route = JSON.parse(event.data);
            }
            nextData = 'EMPTY';
        }
    } else {
        if (event.data !== CONNECT && event.data !== RECEIVE) {
            if (event.data === SENDING) {
                waiting = true;
            } else if (event.data === DISCONNECT) {
                alert(DISCONNECT);
            }
        }
    }
}

function onClose(event) {
    alert('Closed');
}

function send(text) {
    webSocket.send(text);
}

function closeSocket() {
    webSocket.close();
}